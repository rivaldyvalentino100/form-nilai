package com.example.nilai

abstract class Mahasiswa {
    abstract fun CountValue(persensi : Int, tugas : Int, uts : Int, uas : Int) : Int

    abstract fun GradeValue(resultCountValue : Int) : String
}